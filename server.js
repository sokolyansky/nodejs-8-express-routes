'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/post', (req, res, next) => {
  if (!req.headers.key) res.sendStatus(401);

  next();
});
app.use('/post', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    throw new Error('Body is absent in request!!!');
  }

  next();
});

app.get('/', (req, res) => {
  res.json('Hello Express.js');
});
app.get('/hello', (req, res) => {
  res.json('Hello stranger !');
});
app.get('/hello/*', (req, res) => {
  res.json(`Hello, ${req.params[0]} !`);
});
app.all('/sub/*/*', (req, res) => {
  res.json(`You requested URI: ${req.protocol}://${req.headers.host}${req.url} `);
});
app.post('/post', (req, res) => {
  res.send(req.body);
});


app.use((err, req, res, next) => {

  res.status(404).json(err);

});
var ser = app.listen(port, () => {
  console.log('HTTP server has started on %d port ', port);
});